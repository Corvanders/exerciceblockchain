import hashlib

NONCE_LIMIT = 10000000000

zeroes = 6


def mine(block_number, transactions, previous_hash):
    for nonce in range(NONCE_LIMIT):
        base_text = str(block_number) + transactions + previous_hash + str(nonce)
        hash_try = hashlib.sha256(base_text.encode()).hexdigest()
        if hash_try.startswith('0' * zeroes):
            print(f"Vous avez du hopecoin: {nonce} HopeCoins")
            return hash_try

    return -1


block_number = 24
transactions = "76123fccv32132"
previous_hash = "2161546165651165"

mine(block_number, transactions, previous_hash)


