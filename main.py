"""""
Exercice Blockchain
Longueville François
Hope version 1.0
"""""

import hashlib


class Hope:
    def __init__(self, previous_block_hash, transaction_list):
        self.previous_block_hash = previous_block_hash
        self.transaction_list = transaction_list

        self.block_data = f"{' - '.join(transaction_list)} - {previous_block_hash}"
        self.block_hash = hashlib.sha256(self.block_data.encode()).hexdigest()


class Blockchain:

    def __init__(self):
        self.chain = []
        self.generate_genesis_block()

    def generate_genesis_block(self):
        self.chain.append(Hope("0", ['Hope']))

    def create_block_from_transaction(self, transaction_list):
        previous_block_hash = self.last_block.block_hash
        self.chain.append(Hope(previous_block_hash, transaction_list))

    def display_chain(self):
        for i in range(len(self.chain)):
            print(f"Data {i + 1}: {self.chain[i].block_data}")
            print(f"Hash {i + 1}: {self.chain[i].block_hash}\n")

    @property
    def last_block(self):
        return self.chain[-1]


t1 = "Francois sends 3.1 to Alfred"
t2 = "Francois sends 6.1 to Lolo"
t3 = "Francois sends 10.1 to Bruno"
t4 = "Francois sends 42.1 to  Bob"
t5 = "Francois sends 34.1 to Kevin"

myblockchain = Blockchain()

myblockchain.create_block_from_transaction(t1)
myblockchain.create_block_from_transaction(t2)
myblockchain.create_block_from_transaction(t3)
myblockchain.create_block_from_transaction(t4)
myblockchain.create_block_from_transaction(t5)


myblockchain.display_chain()
